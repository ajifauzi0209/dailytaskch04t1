package com.kazakimaru.dailytaskch04t1

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.annotation.LayoutRes
import com.google.android.material.snackbar.Snackbar
import com.kazakimaru.dailytaskch04t1.databinding.FragmentChallengeBinding


class ChallengeFragment : Fragment() {

    private var _binding: FragmentChallengeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentChallengeBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToast()
        showSnackbar()
    }

    private fun myToast(pesan: String): Toast {
        return Toast.makeText(requireContext(), pesan, Toast.LENGTH_SHORT)
    }

    private fun showToast() {
        binding.btnShowToast.setOnClickListener {
            val getNama = binding.editNama.editText?.text.toString()
            if (getNama.isEmpty()) {
                binding.editNama.error = "Nama tidak boleh kosong"
            } else {
                binding.editNama.isErrorEnabled = false
                myToast("Selamat Datang $getNama").show()
            }
        }
    }

    private fun Snackbar.addAction(@LayoutRes aLayoutId: Int, aLabel: String, aListener: View.OnClickListener?) : Snackbar {
        // Add button
        val button = LayoutInflater.from(view.context).inflate(aLayoutId, null) as Button
        view.findViewById<Button>(com.google.android.material.R.id.snackbar_action).let {
            // Copy layout
            button.layoutParams = it.layoutParams
            // Copy colors
            (button as? Button)?.setTextColor(it.textColors)
            (it.parent as? ViewGroup)?.addView(button)
        }
        button.text = aLabel
        button.setOnClickListener {
            this.dismiss()
            aListener?.onClick(it)
        }
        return this
    }

    private fun showSnackbar() {
        binding.btnShowSnackbar.setOnClickListener {
            val snackBar = Snackbar.make(it, "Short & Dismiss", Snackbar.LENGTH_INDEFINITE)
            snackBar.setAction(R.string.toast_short) {
                myToast("Ini Toast Short").show()
            }
            snackBar.addAction(R.layout.snackbar_extra_button, "Dismiss") {
                snackBar.dismiss()
            }
            snackBar.show()
        }
    }
}